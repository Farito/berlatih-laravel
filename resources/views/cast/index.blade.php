@extends('layout.master')

@section('judul')
    List Pemain Film
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
              <form action="/cast/{{$item->id}}" method="post">
              <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @method('delete')
              @csrf
              <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
            </td>
        </tr>
      @empty
          <tr>
            <td>Data masih kosong!</td>
          </tr>
      @endforelse
    </tbody>
</table>
  
@endsection