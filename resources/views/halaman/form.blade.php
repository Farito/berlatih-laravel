<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
</head>
<body>
<h1>Buat Akun Baru!</h1>
    <h2> Sign Up Form</h2>
    <form action="/welcome" method="post">
    @csrf 
        <label for="fname">First Name</label> <br><br>
        <input type="text" name="fname"> <br><br>
        <label for="lname">Last Name</label> <br><br>
        <input type="text" name="lname"> <br><br>
    <p>Gender:</p>
            <input type="radio" name="Gender" id="male" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" name="Gender" id="female" value="Female">
            <label for="female">Female</label><br>
            <input type="radio" name="Gender" id="other" value="Other">
            <label for="other">Other</label><br>
    <p> Nationality:</p>
            <select>
              <option>Indonesian</option>  
              <option>United State</option>
              <option>Saudi Arabia</option>
              <option>Russian</option>
              <option>Japan</option>
            </select>
        
    <p>Language Spoken</p>
            <input type="checkbox" id="lang1" value="Bahasa Indonesia">
            <label for="lang1">Bahasa Indonesia</label><br>
            <input type="checkbox" id="lang2" value="English">
            <label for="lang2">English</label><br>
            <input type="checkbox" id="lang3" value="Japan">
            <label for="lang3">Japan</label><br>
            <input type="checkbox" id="lang4" value="Other">
            <label for="lang4">Other</label><br>
    <p>Bio:</p>
    <textarea cols="21" rows="10"></textarea>
    <br>
    
    <input type="submit" value="Sign Up">

    </form>
    
</body>
</html>